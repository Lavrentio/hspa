import { Login } from "./Login";

export interface User extends Login {
  email: string;
  phone: string;
}
