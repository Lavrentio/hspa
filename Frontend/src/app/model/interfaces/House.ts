import { InteractionTypes } from "../../common/enums/InteractionType";

export interface House {
    id: number;
    name: string;
    type: string;
    price: number;
    interactionType: InteractionTypes,
    imageUrl?: string;
}
