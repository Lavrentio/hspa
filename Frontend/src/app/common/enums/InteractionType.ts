export enum InteractionTypes {
  None = 0,
  ForSell = 1,
  ForRent = 2
}
