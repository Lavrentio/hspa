import { AbstractControl, ValidationErrors } from "@angular/forms";

export class PasswordValidator {
  static passwordsShouldMatch(control: AbstractControl): ValidationErrors | null {
    let newPassword = control.get('password');
    let confirmPassword = control.get('confirmPassword');

    if (newPassword?.value != confirmPassword?.value) {
      return { passShouldMatch: true }
    }
    return null;
  }
}
