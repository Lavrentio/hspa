import { ErrorHandler } from "@angular/core";

export class AppErrorHandler extends ErrorHandler {
  override handleError(error: any): void {
    alert("What`s wrong with you?!"),
      console.log(error);
  }
}
