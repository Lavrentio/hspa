import { Component, Input } from "@angular/core";
import { House } from "../../model/interfaces/House";

@Component({
    selector: 'app-property-card',
    templateUrl: 'property-card.component.html',
    styleUrls: ['property-card.component.css']
})


export class PropertyCardComponent {

    @Input()
    property!: House;

}


