import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { InteractionTypes } from 'src/app/common/enums/InteractionType';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {
  interactionTypes = InteractionTypes;
  keys: number[] = [];

  @ViewChild('Form') addPropertyForm!: NgForm;

  constructor() { }

  ngOnInit() {
    this.keys = Object.keys(this.interactionTypes)
      .filter(key => !isNaN(Number(key)) && Number(key) > 0)
      .map(key => +key);
  }

  onSubmit() {
    console.log(this.addPropertyForm);
  }

  @ViewChild('formTabs') formTabs?: TabsetComponent;

  selectTab(tabId: number) {
    if (this.formTabs?.tabs[tabId]) {
      this.formTabs.tabs[tabId].active = true;
    }
  }
}
