import { House } from '../../model/interfaces/House';
import { Component, OnInit } from '@angular/core';
import { HousingService } from 'src/app/services/housing.service';
import { ActivatedRoute } from '@angular/router';
import { InteractionTypes } from 'src/app/common/enums/InteractionType';

@Component({
    selector: 'app-propery-list',
    templateUrl: './propery-list.component.html',
    styleUrls: ['./propery-list.component.css']
})
export class ProperyListComponent implements OnInit {

    interactionType: InteractionTypes = InteractionTypes.None;
    properties!: House[];

    constructor(
        private route: ActivatedRoute,
        private housingService: HousingService) { }

    ngOnInit() {
        this.initInteractionType();

        console.log(this.interactionType);

        if (this.interactionType === InteractionTypes.None) {
            this.housingService.getAll()
                .subscribe(p => this.properties = p);
        } else {
            this.housingService.getHousesByInteractionType(this.interactionType)
                .subscribe(p => this.properties = p);
        }
    }

    private initInteractionType() {
        let url: string = this.route.snapshot.url.toString();
        console.log(url);
        if (url == 'buy-property') {
            this.interactionType = InteractionTypes.ForSell;
        } else if (url == 'rent-property') {
            this.interactionType = InteractionTypes.ForRent;
        }
        console.log(this.interactionType);

    }
}
