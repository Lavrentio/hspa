import { Injectable } from '@angular/core';
import { Login } from '../model/interfaces/Login';
import { User } from '../model/interfaces/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  authUser(user: Login) {
    let users: User[] = [];
    let data: string | null = localStorage.getItem('Users');

    if (!data) {
      throw new Error("There are no users in the storage");
    }

    users = JSON.parse(data ?? '');
    return users.find((u: User) => u.name == user.name && u.password == user.password);
  }
}
