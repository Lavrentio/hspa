import { House } from '../model/interfaces/House';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { InteractionTypes } from '../common/enums/InteractionType';
import { filter, map, Observable, Subscriber } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HousingService extends DataService<House> {

    constructor(http: HttpClient) {
        super('data/properties.json', http);
    }

    getHousesByInteractionType(type: InteractionTypes): Observable<House[]> {
        return this.getAll().pipe(
            map(h => {
                let houses: House[] = [];
                for (const house of h) {
                    if (house.interactionType == type) {
                        houses.push(house);
                    }
                }
                return houses;
            })
        );
    }
}
