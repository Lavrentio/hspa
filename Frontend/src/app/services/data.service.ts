import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService<T> {
  constructor(@Inject(String) private url: string, private http: HttpClient) { }

  getAll(): Observable<T[]> {
    return this.http.get(this.url)
      .pipe(
        map(response => response as T[]),
        catchError(this.handleError));
  }

  private handleError(error: Response) {
    return throwError(() => new Error(error.toString()));
  }
}
