import { Injectable } from '@angular/core';
import * as alertify from 'alertifyjs';


@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

  constructor() { }

  printSuccess(message: string) {
    alertify.success(message);
  }

  printError(message: string) {
    alertify.error(message);
  }

  printWarning(message: string) {
    alertify.warning(message);
  }
}
