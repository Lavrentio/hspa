import { Injectable } from '@angular/core';
import { User } from '../model/interfaces/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  addUser(user: User) {
    let users: User[] = [];
    let data: string | null = localStorage.getItem('Users');
    if (data) {
      users = JSON.parse(data ?? '');
      users = [...users, user];
    } else {
      users = [user];
    }

    localStorage.setItem('Users', JSON.stringify(users));
  }

}
