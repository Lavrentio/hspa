import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from 'src/app/model/interfaces/Login';
import { User } from 'src/app/model/interfaces/User';
import { AlertifyService } from 'src/app/services/alertify.service';
import { AuthService } from 'src/app/services/auth.service';
import { BsDropdownConfig } from 'ngx-bootstrap/dropdown';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  form!: FormGroup;
  login!: Login;

  constructor(private fb: FormBuilder,
    private alertifyService: AlertifyService,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  get name() {
    return this.form.get('name')
  }

  get password() {
    return this.form.get('password')
  }

  private createForm() {
    this.form = this.fb.group({
      name: [null, Validators.required],
      password: [null, Validators.required]
    })
  }

  onLogin() {
    if (this.form.invalid) {
      this.printLoginError();
    } else {
      this.maplogin();
      const token = this.authService.authUser(this.login);
      this.checkUser(token);
      this.form.reset();

      this.router.navigate(['/']);
    }
  }

  private maplogin() {
    this.login = {
      name: this.form.value.name,
      password: this.form.value.password,
    }
  }

  private printLoginError() {
    this.alertifyService.printError('Invalid login and/or password');
  }

  private checkUser(token: any) {
    if (token) {
      localStorage.setItem('token', token.name);
    } else {
      this.printLoginError();
    }
  }
}
