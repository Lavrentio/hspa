import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/model/interfaces/User';
import { PasswordValidator } from 'src/app/common/validators/password.validators';
import { UserService } from 'src/app/services/user.service';
import { AlertifyService } from 'src/app/services/alertify.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  form!: FormGroup;
  user!: User;
  formSubmitted!: boolean;

  constructor(private fb: FormBuilder,
    private userService: UserService,
    private alertifyService: AlertifyService) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.form = this.fb.group({
      name: [null,
        [Validators.required,
        Validators.maxLength(30)]
      ],
      email: [null,
        [Validators.required,
        Validators.email]
      ],
      password: [null,
        [Validators.required,
        Validators.minLength(6)]
      ],
      confirmPassword: [null,
        [Validators.required]
      ],
      phone: [null,
        [Validators.required,
        Validators.pattern("^\\d{3}-\\d{3}-\\d{4}$")]
      ]
    },
      {
        validators: PasswordValidator.passwordsShouldMatch
      });
  }

  get name() {
    return this.form.get('name');
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  get phone() {
    return this.form.get('phone');
  }

  onSubmit() {
    this.formSubmitted = true;
    if (this.form.invalid) {
      this.alertifyService.printError('Provide the required fields, please');
    } else {
      this.userService.addUser(this.mapUserData());
      this.form.reset();
      this.formSubmitted = false;
      this.alertifyService.printSuccess(`Congrats, ${this.user.name}! You are successfully registered`);
    }
  }

  private mapUserData(): User {
    return this.user = {
      name: this.name?.value,
      email: this.email?.value,
      password: this.password?.value,
      phone: this.phone?.value
    }
  }
}
